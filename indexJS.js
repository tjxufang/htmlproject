function clickMe() {
    console.log('Clicked by mark')
    alert('welcome')
}


let grades = [21, 52, 73, 44, 85, 65, 47, 28, 39, 92]

let goodGrades = []
grades.forEach(
    function (value){
        if (value > 50){
            // console.log(value)
            goodGrades.push(value)
        }
    }
)
// console.log(goodGrades)

let goodGrades2 = grades.filter(value => value>50)
// let goodGrades22 = grades.filter((value, index) => {
//     console.log(value, index)
//     return value>50
// })
// console.log('good grades', goodGrades22)

let firstGoodGrade = grades.find(value => {
    return value > 50
})
// console.log('first good grade', firstGoodGrade)

let gpa = grades.map(function (value) {
    return value/10
})
// console.log('gpa', gpa)

let sortedGrades = grades.sort()
// console.log('sorted grades1', sortedGrades)

let carMakes = [
    {name: 'bmw', price: '4'},
    {name: 'benz', price: '3'},
    {name: 'toyota', price: '1'},
    {name: 'audi', price: '2'}
]
let sortCars = carMakes.sort(function (a, b) {
    return b.price - a.price
}).map(function (make) {
    return make.name
})
// console.log(sortCars)