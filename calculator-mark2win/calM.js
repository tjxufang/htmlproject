let mem = 0
let op = -1

function ckBtn(btn) {
    console.log('you press:', btn)
    if(!isNaN(btn)) {
        console.log('you press a number')
        pressNumber(btn)
    } else if ('AC' === btn) {
        clear()
    } else if ('.' === btn) {
        addPoint()
    } else if ('→' === btn) {
        backforward()
    } else if ('π' === btn ) {
        setTextBox("3.1415926")
    } else if ('√' === btn) {
        setTextBox(Math.sqrt(getTextBoxValue()))
    } else if ('=' === btn) {
        console.log('you press:', btn)
        requestAnswer()

    } else {
        mem = getTextBoxValue()
        setTextBox("0")
        setOperator(btn)
    }
}

function requestAnswer() {
    console.log('request')
    let method = ''
    let num = Number(getTextBoxValue())
    mem = Number(mem)
    let res = 0
    if ( 0 === op) {
        console.log('op add' )
        res = mem + num
        method = '+'
    } else if ( 1 === op) {
        res = mem - num
        method = '-'
    } else if ( 2 === op) {
        console.log('op', op)
        res = mem * num
        method = '*'
    } else if ( 3 === op) {
        res = mem / num
        method = '/'
    }
    setTextBox(res)
    op = -1

    storeData(mem, method, num, res)
    getCurrentData(mem, method, num, res)
}

function pressNumber(btn) {
    if ( '0' === getTextBoxValue()) {
        if ('0' !== btn) {
            setTextBox('' + btn)
        }
    } else {
        setTextBox('' + getTextBoxValue() + btn)
    }
}

function setOperator(btn) {
    const OPS = ['+', '-', '×', '÷']
    let index = OPS.indexOf(btn)
    if (index > -1) {
        op = index
        console.log('x')
    } else {
        console.log('the wrong operator', btn)
    }
}

function clear() {
    setTextBox("0")
}

function setTextBox(num) {
    document.getElementById('displayTextBox').innerHTML = num
}

function getTextBoxValue() {
    return document.getElementById('displayTextBox').innerText
}

function addPoint() {
    let orgNumb = String(getTextBoxValue())
    if (!orgNumb.includes(".")) {
        setTextBox(orgNumb + '.')
    }
}

function backforward() {
    let orgNumb = getTextBoxValue()
    if (orgNumb.length > 1) {
        setTextBox(orgNumb.substr(0, orgNumb.length - 1))
    } else {
        setTextBox("0")
    }
}

let storage = ''
let i_key = 1
function storeData(mem, method, num, res){
    if (storage === '' && localStorage.getItem('data') !== null){
        storage += localStorage.getItem('data') + i_key + ': ' + mem + method + num + '=' + res + ';'
    } else {
        storage += i_key + ':' + mem + method + num + '=' + res + ';'
    }
    localStorage.setItem('data', storage)
    i_key ++
}
let line = 1
function getCurrentData(mem, method, num, res){
    let table = document.getElementById('myTableData')
    let rowCount = table.rows.length
    let row = table.insertRow(rowCount)
    row.insertCell(0).innerHTML = line.toString()
    row.insertCell(1).innerHTML = mem.toString() + method + num + '=' + res
    line++
}
let eleStorage = document.getElementById('historyData')
function restoreData(){
    let data = localStorage.getItem('data').split(';')
    let resDiv = ''
    data.forEach(value => {
        resDiv += value + '<br>'
    })
    eleStorage.innerHTML = resDiv
}

function clearData(){
    localStorage.clear()
    eleStorage.innerHTML = ''
    storage = ''
}