const loadComReq = new XMLHttpRequest()  //defines a new XHRequest
const commentURL = 'http://jsonplaceholder.typicode.com/comments'
const max =50

loadComReq.onerror = () => {  //when failed
    console.log(loadComReq.response, loadComReq.response.code)
}

function fetch(id){
    loadComReq.open('GET', commentURL +`/${id}`)
    loadComReq.send()
}

let loadingImg = document.getElementById('loading')
function showLoading() {
    loadingImg.style.visibility = "visible"
}

function removeLoading() {
    loadingImg.style.visibility = "hidden"
}

let recordId = 0
let interval = '' // declare here so that can be cleared
function loading() {
    showLoading()
    interval = setInterval(function () {
        if (recordId>max){
            stopShow()
        }
        let bodyOffset = document.body.offsetHeight
        console.log(bodyOffset+'/'+innerHeight)
        if (bodyOffset>window.innerHeight){
            stopShow()
        }

        sendRequest(recordId++)

    }, 500)
}

function stopShow() {
    clearInterval(interval)
}

function sendRequest(recordId) {
    if (recordId < max){
        let table=document.getElementById('table')
        let id = recordId+1 // index+1 to show correct
        fetch(id)
        table.insertAdjacentHTML('beforeend', `<tr id='t${id}'></tr>`)
        loadComReq.onload = () => {  //after server receives (send()), this function is run
            let res = JSON.parse(loadComReq.response) //string->object
            console.log(res)
            appendToTable(res)
        }
    }
}

function appendToTable(obj_js) {
    let html = `
        <td class="column1">${obj_js.id}</td>
        <td class="column1">
        <img class="pic" width="50px" height="auto" src="https://m.media-amazon.com/images/I/51NcnWZlwlL._AA256_.jpg">
        </td> 
            <td class="column2">
            <b>${obj_js.name}</b><br>${obj_js.email}<br>${obj_js.body}
        </td>`
    document.getElementById('t'+obj_js.id).innerHTML = html
    removeLoading()
}

window.addEventListener('scroll', () => {
    console.log(document.documentElement.scrollHeight, window.innerHeight, window.pageYOffset)
    const scrollable = document.documentElement.scrollHeight - window.innerHeight
    const scrolled = window.pageYOffset
    if (scrollable-scrolled < 10){
        sendRequest(recordId++)
    }
})

loading()

// let xhr = new XMLHttpRequest()
// let baseURL = 'http://jsonplaceholder.typicode.com/comments'
//
// function fetch() {
//     xhr.open('GET', baseURL)
//     xhr.send()
// }
//
// xhr.onload = function (){
//     console.log('get data', xhr.response)
// }
//
// xhr.onerror = function (){
//     console.log('error', xhr.response)
// }