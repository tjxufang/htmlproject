let numb1 = document.getElementById('numb1')
let numb2 = document.getElementById('numb2')


function inputCheck(){
    if (numb1.value === '' || numb2.value === ''){
        console.log('disabled')
        changeBtnStates(false)
    } else {
        console.log('enabled')
        changeBtnStates(true)
    }
}


function changeBtnStates(state) {
    let calbtn = document.getElementById('calBtn')
    if (state) {
        calbtn.removeAttribute('disabled')
    } else {
        calbtn.setAttribute('disabled', '')
    }
}



function calc(){
    // debugger
    let parNumb1 = parseFloat(numb1.value)
    let parNumb2 = parseFloat(numb2.value)
    let operator = document.getElementById('operator')
    let ans = document.getElementById('ans')

    if (operator.value === '+'){
        ans.value = parNumb1+parNumb2
    } else if (operator.value === '-'){
        ans.value = parNumb1-parNumb2
    } else if (operator.value === '*'){
        ans.value = parNumb1*parNumb2
    } else if (operator.value === '/'){
        ans.value = parNumb1/parNumb2
    }
}

