const numbBtn = document.querySelectorAll('[data-number]')
const opBtn = document.querySelectorAll('[data-operation]')
const eqBtn = document.querySelector('[data-equals]')
const delBtn = document.querySelector('[data-delete]')
const acBtn = document.querySelector('[data-clear]')

const display = document.querySelector('#display')
const history = document.querySelector('#history')

class Calculator {
    constructor(numb1, numb2) {
        this.numb1 = numb1
        this.numb2 = numb2 // current input
        this.clear() // setting up
    }

    clear() {
        this.numb1 = ''
        this.numb2 = ''
        this.operation = undefined
    }

    delete() {
        console.log('hi')
        this.numb2 = this.numb2.slice(0, this.numb2.length-1)
    }

    appendNumb(number) {
        if (number === '.' && this.numb2.includes('.')) return
        this.numb2 = this.numb2.toString() + number.toString()
        console.log('number',number,'this.curOp', this.numb2)
    }

    chooseOp(operation) {
        if (this.numb2 === '') {
            this.operation = operation
            return // end the function
        }
        if (this.numb1 !== '') {
            this.compute()
        }
        if (this.operation !== ''){
            this.operation = operation
        }
        console.log('operation',operation)
        this.operation = operation
        this.numb1 = this.numb2
        this.numb2 = ''
    }

    compute() {
        let res
        const prev = parseFloat(this.numb1)
        const cur = parseFloat(this.numb2)
        if (isNaN(prev) || isNaN(cur)) return
        switch (this.operation) {
            case '+':
                res = prev + cur
                break
            case '-':
                res = prev - cur
                break
            case '*':
                res = prev * cur
                break
            case '/':
                res = prev / cur
                break
            default:
                return
        }
        this.updateHistory(this.numb1, this.operation, this.numb2, res)
        this.numb2 = res
        this.operation = undefined
        this.numb1 = ''
        this.updateDisplay()
    }

    updateDisplay() {
        display.value = this.numb2
    }
    updateHistory(n1, op, n2, res) {
        if (history.value !== '') {
            history.value = history.value + n1+op+n2+'='+res
        }
        history.value = n1+op+n2+'='+res
    }
}

const calculator = new Calculator('', '')

numbBtn.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumb(button.value)
        calculator.updateDisplay()
    })
})

opBtn.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOp(button.value)
        // calculator.updateDisplay()
    })
})

delBtn.addEventListener('click', () => {
    calculator.delete()
    calculator.updateDisplay()
})

acBtn.addEventListener('click', () => {
    calculator.clear()
    calculator.updateDisplay()
})

eqBtn.addEventListener('click', () => {
    calculator.compute()
    calculator.updateDisplay()
})